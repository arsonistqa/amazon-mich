class HomePage < SitePrism::Page
  element :search_field, '#twotabsearchtextbox'
  element :search_button, '.nav-search-submit input'
  element :lov_price_field, '#low-price'
  element :go_button, '.leftNavGoBtn'

  def select_category(name)
    find('#searchDropdownBox').find("[value='search-alias=#{name}']").select_option
  end
end
