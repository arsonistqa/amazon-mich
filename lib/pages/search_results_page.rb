class SearchResultsPage < SitePrism::Page

  require 'csv'

  element :see_more_button, '[data-el-to-show="#seeAllDepartmentOpen1"]'

  def get_data(file)
    CSV.open("amazon_files/#{file}", 'wb') do |csv|
      results = find('#s-result-count').text
      on_one_page = results.split(' ').first
      if file.include? 'fr_'
        on_all_pages = results.split(' sur ')[1]
        on_all_pages = on_all_pages.split(' résultats ').first
      else
        on_all_pages = results.split(' ')[2]
      end
      on_one_page = on_one_page.split('-').last.to_i

      if on_all_pages.include? ','
        on_all_pages = on_all_pages.split(',').join.to_i
      else
        if on_all_pages.include? '.'
          on_all_pages = on_all_pages.split('.').join.to_i
        else
          on_all_pages = on_all_pages.split(' ').join.to_i
        end
      end

      pagination = on_all_pages / 100 * 0.5 / on_one_page

      if pagination > 400
        pagination = 400
      end

      if file.include? 'co_uk_stripbooks'
        if pagination > 75
          pagination = 75
        end
      end

      for i in 1..pagination.to_i do
        sleep 5
        all('.s-item-container').each do |product|
          asin = nil
          price = nil
          begin
            asin = product.find('.s-access-detail-page')['href'].split('/dp/').last.split('/').first
          rescue Exception => e
            asin = 'NO ASIN FOR THIS PRODUCT'
          end
          begin
            price = product.find(:xpath, './/i[@aria-label="Prime"]/..//span[contains(@class,"s-price")]').text
          rescue Exception => e
            begin
              price = product.find(:xpath, './/*[contains(@class,"s-price a-text-bold")]').text
            rescue Exception => e
            end
          end
          begin
            unless price.include? '-'
              if price.include? '£'
                price = price.gsub('£','')
              else
                price = price.gsub('EUR ','')
              end
            else
              price = price.split(' - ').first
              if price.include? '£'
                price = price.gsub('£','')
              else
                price = price.gsub('EUR ','')
              end
            end
          rescue Exception => e
            price = 'NO PRICE FOR THIS PRODUCT'
          end
          csv << [asin, price]
          puts asin
          puts price
        end
        begin
          find('#pagnNextString').click
        rescue Exception => e
          execute_script("document.getElementById('pagnNextString').click()")
        end
      end
    end
  end
end
