class Amazon

  def home_page
    HomePage.new
  end

  def search_results_page
    SearchResultsPage.new
  end
end
