@tests
Feature: It

  Scenario: It - baby
    Given I navigate on 'it' domain
    When I select 'baby' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_baby.csv' file

  Scenario: It - beauty
    Given I navigate on 'it' domain
    When I select 'beauty' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_beauty.csv' file

  Scenario: It - stripbooks
    Given I navigate on 'it' domain
    When I select 'stripbooks' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_stripbooks.csv' file

  Scenario: It - automotive
    Given I navigate on 'it' domain
    When I select 'automotive' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_automotive.csv' file

  Scenario: It - popular
    Given I navigate on 'it' domain
    When I select 'popular' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_popular.csv' file

  Scenario: It - classical
    Given I navigate on 'it' domain
    When I select 'classical' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_classical.csv' file

  Scenario: It - computers
    Given I navigate on 'it' domain
    When I select 'computers' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_computers.csv' file

  Scenario: It - digital-music
    Given I navigate on 'it' domain
    When I select 'digital-music' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_digital-music.csv' file

  Scenario: It - diy
    Given I navigate on 'it' domain
    When I select 'diy' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_diy.csv' file

  Scenario: It - dvd
    Given I navigate on 'it' domain
    When I select 'dvd' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_dvd.csv' file

  Scenario: It - electronics
    Given I navigate on 'it' domain
    When I select 'electronics' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_electronics.csv' file

  Scenario: It - outdoor
    Given I navigate on 'it' domain
    When I select 'outdoor' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_outdoor.csv' file

  Scenario: It - handmade
    Given I navigate on 'it' domain
    When I select 'handmade' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_handmade.csv' file

  Scenario: It - drugstore
    Given I navigate on 'it' domain
    When I select 'drugstore' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_drugstore.csv' file

  Scenario: It - kitchen
    Given I navigate on 'it' domain
    When I select 'kitchen' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_kitchen.csv' file

  Scenario: It - industrial
    Given I navigate on 'it' domain
    When I select 'industrial' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_industrial.csv' file

  Scenario: It - jewelry
    Given I navigate on 'it' domain
    When I select 'jewelry' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_jewelry.csv' file

  Scenario: It - appliances
    Given I navigate on 'it' domain
    When I select 'appliances' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_appliances.csv' file

  Scenario: It - lighting
    Given I navigate on 'it' domain
    When I select 'lighting' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_lighting.csv' file

  Scenario: It - dvd-bypos
    Given I navigate on 'it' domain
    When I select 'dvd-bypos' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_dvd-bypos.csv' file

  Scenario: It - luggage
    Given I navigate on 'it' domain
    When I select 'luggage' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_mluggage.csv' file

  Scenario: It - luxury-beauty
    Given I navigate on 'it' domain
    When I select 'luxury-beauty' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_luxury-beauty.csv' file

  Scenario: It - mi
    Given I navigate on 'it' domain
    When I select 'mi' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_mi.csv' file

  Scenario: It - videogames
    Given I navigate on 'it' domain
    When I select 'videogames' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_videogames.csv' file

  Scenario: It - pets
    Given I navigate on 'it' domain
    When I select 'pets' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_pets.csv' file

  Scenario: It - shoes
    Given I navigate on 'it' domain
    When I select 'shoes' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_shoes.csv' file

  Scenario: It - software
    Given I navigate on 'it' domain
    When I select 'software' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_software.csv' file

  Scenario: It - sports
    Given I navigate on 'it' domain
    When I select 'sports' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_sports.csv' file

  Scenario: It - office-products
    Given I navigate on 'it' domain
    When I select 'office-products' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_office-products.csv' file

  Scenario: It - toys
    Given I navigate on 'it' domain
    When I select 'toys' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_toys.csv' file

  Scenario: It - vhs
    Given I navigate on 'it' domain
    When I select 'vhs' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_vhs.csv' file

  Scenario: CoUk - watches
    Given I navigate on 'it' domain
    When I select 'watches' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'it_watches.csv' file