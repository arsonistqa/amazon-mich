@tests
Feature: Es

  Scenario: Es - baby
    Given I navigate on 'es' domain
    When I select 'baby' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_baby.csv' file

  Scenario: Es - beauty
    Given I navigate on 'es' domain
    When I select 'beauty' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_beauty.csv' file

  Scenario: Es - stripbooks
    Given I navigate on 'es' domain
    When I select 'stripbooks' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_stripbooks.csv' file

  Scenario: Es - automotive
    Given I navigate on 'es' domain
    When I select 'automotive' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_automotive.csv' file

  Scenario: Es - popular
    Given I navigate on 'es' domain
    When I select 'popular' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_popular.csv' file

  Scenario: Es - classical
    Given I navigate on 'es' domain
    When I select 'classical' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_classical.csv' file

  Scenario: Es - computers
    Given I navigate on 'es' domain
    When I select 'computers' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_computers.csv' file

  Scenario: Es - digital-music
    Given I navigate on 'es' domain
    When I select 'digital-music' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_digital-music.csv' file

  Scenario: Es - diy
    Given I navigate on 'es' domain
    When I select 'diy' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_diy.csv' file

  Scenario: Es - dvd
    Given I navigate on 'es' domain
    When I select 'dvd' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_dvd.csv' file

  Scenario: Es - electronics
    Given I navigate on 'es' domain
    When I select 'electronics' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_electronics.csv' file

  Scenario: Es - outdoor
    Given I navigate on 'es' domain
    When I select 'outdoor' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_outdoor.csv' file

  Scenario: Es - handmade
    Given I navigate on 'es' domain
    When I select 'handmade' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_handmade.csv' file

  Scenario: Es - drugstore
    Given I navigate on 'es' domain
    When I select 'drugstore' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_drugstore.csv' file

  Scenario: Es - kitchen
    Given I navigate on 'es' domain
    When I select 'kitchen' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_kitchen.csv' file

  Scenario: Es - industrial
    Given I navigate on 'es' domain
    When I select 'industrial' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_industrial.csv' file

  Scenario: Es - jewelry
    Given I navigate on 'es' domain
    When I select 'jewelry' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_jewelry.csv' file

  Scenario: Es - appliances
    Given I navigate on 'es' domain
    When I select 'appliances' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_appliances.csv' file

  Scenario: Es - lighting
    Given I navigate on 'es' domain
    When I select 'lighting' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_lighting.csv' file

  Scenario: Es - dvd-bypos
    Given I navigate on 'es' domain
    When I select 'dvd-bypos' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_dvd-bypos.csv' file

  Scenario: Es - luggage
    Given I navigate on 'es' domain
    When I select 'luggage' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_mluggage.csv' file

  Scenario: Es - luxury-beauty
    Given I navigate on 'es' domain
    When I select 'luxury-beauty' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_luxury-beauty.csv' file

  Scenario: Es - mi
    Given I navigate on 'es' domain
    When I select 'mi' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_mi.csv' file

  Scenario: Es - videogames
    Given I navigate on 'es' domain
    When I select 'videogames' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_videogames.csv' file

  Scenario: Es - pets
    Given I navigate on 'es' domain
    When I select 'pets' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_pets.csv' file

  Scenario: Es - shoes
    Given I navigate on 'es' domain
    When I select 'shoes' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_shoes.csv' file

  Scenario: Es - software
    Given I navigate on 'es' domain
    When I select 'software' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_software.csv' file

  Scenario: Es - sports
    Given I navigate on 'es' domain
    When I select 'sports' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_sports.csv' file

  Scenario: Es - office-products
    Given I navigate on 'es' domain
    When I select 'office-products' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_office-products.csv' file

  Scenario: Es - toys
    Given I navigate on 'es' domain
    When I select 'toys' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_toys.csv' file

  Scenario: Es - vhs
    Given I navigate on 'es' domain
    When I select 'vhs' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_vhs.csv' file

  Scenario: Es - watches
    Given I navigate on 'es' domain
    When I select 'watches' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'es_watches.csv' file