@tests
Feature: CoUk

  Scenario: CoUk - baby
    Given I navigate on 'co.uk' domain
    When I select 'baby' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_baby.csv' file

  Scenario: CoUk - beauty
    Given I navigate on 'co.uk' domain
    When I select 'beauty' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_beauty.csv' file

  Scenario: CoUk - stripbooks
    Given I navigate on 'co.uk' domain
    When I select 'stripbooks' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_stripbooks.csv' file

  Scenario: CoUk - automotive
    Given I navigate on 'co.uk' domain
    When I select 'automotive' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_automotive.csv' file

  Scenario: CoUk - popular
    Given I navigate on 'co.uk' domain
    When I select 'popular' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_popular.csv' file

  Scenario: CoUk - classical
    Given I navigate on 'co.uk' domain
    When I select 'classical' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_classical.csv' file

  Scenario: CoUk - computers
    Given I navigate on 'co.uk' domain
    When I select 'computers' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_computers.csv' file

  Scenario: CoUk - digital-music
    Given I navigate on 'co.uk' domain
    When I select 'digital-music' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_digital-music.csv' file

  Scenario: CoUk - diy
    Given I navigate on 'co.uk' domain
    When I select 'diy' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_diy.csv' file

  Scenario: CoUk - dvd
    Given I navigate on 'co.uk' domain
    When I select 'dvd' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_dvd.csv' file

  Scenario: CoUk - electronics
    Given I navigate on 'co.uk' domain
    When I select 'electronics' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_electronics.csv' file

  Scenario: CoUk - outdoor
    Given I navigate on 'co.uk' domain
    When I select 'outdoor' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_outdoor.csv' file

  Scenario: CoUk - handmade
    Given I navigate on 'co.uk' domain
    When I select 'handmade' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_handmade.csv' file

  Scenario: CoUk - drugstore
    Given I navigate on 'co.uk' domain
    When I select 'drugstore' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_drugstore.csv' file

  Scenario: CoUk - kitchen
    Given I navigate on 'co.uk' domain
    When I select 'kitchen' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_kitchen.csv' file

  Scenario: CoUk - industrial
    Given I navigate on 'co.uk' domain
    When I select 'industrial' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_industrial.csv' file

  Scenario: CoUk - jewelry
    Given I navigate on 'co.uk' domain
    When I select 'jewelry' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_jewelry.csv' file

  Scenario: CoUk - appliances
    Given I navigate on 'co.uk' domain
    When I select 'appliances' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_appliances.csv' file

  Scenario: CoUk - lighting
    Given I navigate on 'co.uk' domain
    When I select 'lighting' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_lighting.csv' file

  Scenario: CoUk - dvd-bypos
    Given I navigate on 'co.uk' domain
    When I select 'dvd-bypos' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_dvd-bypos.csv' file

  Scenario: CoUk - luggage
    Given I navigate on 'co.uk' domain
    When I select 'luggage' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_mluggage.csv' file

  Scenario: CoUk - luxury-beauty
    Given I navigate on 'co.uk' domain
    When I select 'luxury-beauty' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_luxury-beauty.csv' file

  Scenario: CoUk - mi
    Given I navigate on 'co.uk' domain
    When I select 'mi' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_mi.csv' file

  Scenario: CoUk - videogames
    Given I navigate on 'co.uk' domain
    When I select 'videogames' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_videogames.csv' file

  Scenario: CoUk - pets
    Given I navigate on 'co.uk' domain
    When I select 'pets' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_pets.csv' file

  Scenario: CoUk - shoes
    Given I navigate on 'co.uk' domain
    When I select 'shoes' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_shoes.csv' file

  Scenario: CoUk - software
    Given I navigate on 'co.uk' domain
    When I select 'software' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_software.csv' file

  Scenario: CoUk - sports
    Given I navigate on 'co.uk' domain
    When I select 'sports' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_sports.csv' file

  Scenario: CoUk - office-products
    Given I navigate on 'co.uk' domain
    When I select 'office-products' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_office-products.csv' file

  Scenario: CoUk - toys
    Given I navigate on 'co.uk' domain
    When I select 'toys' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_toys.csv' file

  Scenario: CoUk - vhs
    Given I navigate on 'co.uk' domain
    When I select 'vhs' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_vhs.csv' file

  Scenario: CoUk - watches
    Given I navigate on 'co.uk' domain
    When I select 'watches' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'co_uk_watches.csv' file


