@tests
Feature: Fr

  Scenario: Fr - baby
    Given I navigate on 'fr' domain
    When I select 'baby' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_baby.csv' file

  Scenario: Fr - beauty
    Given I navigate on 'fr' domain
    When I select 'beauty' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_beauty.csv' file

  Scenario: Fr - stripbooks
    Given I navigate on 'fr' domain
    When I select 'stripbooks' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_stripbooks.csv' file

  Scenario: Fr - automotive
    Given I navigate on 'fr' domain
    When I select 'automotive' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_automotive.csv' file

  Scenario: Fr - popular
    Given I navigate on 'fr' domain
    When I select 'popular' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_popular.csv' file

  Scenario: Fr - classical
    Given I navigate on 'fr' domain
    When I select 'classical' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_classical.csv' file

  Scenario: Fr - computers
    Given I navigate on 'fr' domain
    When I select 'computers' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_computers.csv' file

  Scenario: Fr - digital-music
    Given I navigate on 'fr' domain
    When I select 'digital-music' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_digital-music.csv' file

  Scenario: Fr - diy
    Given I navigate on 'fr' domain
    When I select 'diy' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_diy.csv' file

  Scenario: Fr - dvd
    Given I navigate on 'fr' domain
    When I select 'dvd' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_dvd.csv' file

  Scenario: Fr - electronics
    Given I navigate on 'fr' domain
    When I select 'electronics' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_electronics.csv' file

  Scenario: Fr - outdoor
    Given I navigate on 'fr' domain
    When I select 'outdoor' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_outdoor.csv' file

  Scenario: Fr - handmade
    Given I navigate on 'fr' domain
    When I select 'handmade' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_handmade.csv' file

  Scenario: Fr - drugstore
    Given I navigate on 'fr' domain
    When I select 'drugstore' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_drugstore.csv' file

  Scenario: Fr - kitchen
    Given I navigate on 'fr' domain
    When I select 'kitchen' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_kitchen.csv' file

  Scenario: Fr - industrial
    Given I navigate on 'fr' domain
    When I select 'industrial' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_industrial.csv' file

  Scenario: Fr - jewelry
    Given I navigate on 'fr' domain
    When I select 'jewelry' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_jewelry.csv' file

  Scenario: Fr - appliances
    Given I navigate on 'fr' domain
    When I select 'appliances' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_appliances.csv' file

  Scenario: Fr - lighting
    Given I navigate on 'fr' domain
    When I select 'lighting' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_lighting.csv' file

  Scenario: Fr - dvd-bypos
    Given I navigate on 'fr' domain
    When I select 'dvd-bypos' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_dvd-bypos.csv' file

  Scenario: Fr - luggage
    Given I navigate on 'fr' domain
    When I select 'luggage' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_mluggage.csv' file

  Scenario: Fr - luxury-beauty
    Given I navigate on 'fr' domain
    When I select 'luxury-beauty' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_luxury-beauty.csv' file

  Scenario: Fr - mi
    Given I navigate on 'fr' domain
    When I select 'mi' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_mi.csv' file

  Scenario: Fr - videogames
    Given I navigate on 'fr' domain
    When I select 'videogames' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_videogames.csv' file

  Scenario: Fr - pets
    Given I navigate on 'fr' domain
    When I select 'pets' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_pets.csv' file

  Scenario: Fr - shoes
    Given I navigate on 'fr' domain
    When I select 'shoes' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_shoes.csv' file

  Scenario: Fr - software
    Given I navigate on 'fr' domain
    When I select 'software' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_software.csv' file

  Scenario: Fr - sports
    Given I navigate on 'fr' domain
    When I select 'sports' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_sports.csv' file

  Scenario: Fr - office-products
    Given I navigate on 'fr' domain
    When I select 'office-products' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_office-products.csv' file

  Scenario: Fr - toys
    Given I navigate on 'fr' domain
    When I select 'toys' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_toys.csv' file

  Scenario: Fr - vhs
    Given I navigate on 'fr' domain
    When I select 'vhs' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_vhs.csv' file

  Scenario: Fr - watches
    Given I navigate on 'fr' domain
    When I select 'watches' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'fr_watches.csv' file