@tests
Feature: De

  Scenario: De - baby
    Given I navigate on 'de' domain
    When I select 'baby' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_baby.csv' file

  Scenario: De - beauty
    Given I navigate on 'de' domain
    When I select 'beauty' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_beauty.csv' file

  Scenario: De - stripbooks
    Given I navigate on 'de' domain
    When I select 'stripbooks' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_stripbooks.csv' file

  Scenario: De - automotive
    Given I navigate on 'de' domain
    When I select 'automotive' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_automotive.csv' file

  Scenario: De - popular
    Given I navigate on 'de' domain
    When I select 'popular' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_popular.csv' file

  Scenario: De - classical
    Given I navigate on 'de' domain
    When I select 'classical' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_classical.csv' file

  Scenario: De - computers
    Given I navigate on 'de' domain
    When I select 'computers' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_computers.csv' file

  Scenario: De - digital-music
    Given I navigate on 'de' domain
    When I select 'digital-music' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_digital-music.csv' file

  Scenario: De - diy
    Given I navigate on 'de' domain
    When I select 'diy' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_diy.csv' file

  Scenario: De - dvd
    Given I navigate on 'de' domain
    When I select 'dvd' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_dvd.csv' file

  Scenario: De - electronics
    Given I navigate on 'de' domain
    When I select 'electronics' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_electronics.csv' file

  Scenario: De - outdoor
    Given I navigate on 'de' domain
    When I select 'outdoor' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_outdoor.csv' file

  Scenario: De - handmade
    Given I navigate on 'de' domain
    When I select 'handmade' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_handmade.csv' file

  Scenario: De - drugstore
    Given I navigate on 'de' domain
    When I select 'drugstore' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_drugstore.csv' file

  Scenario: De - kitchen
    Given I navigate on 'de' domain
    When I select 'kitchen' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_kitchen.csv' file

  Scenario: De - industrial
    Given I navigate on 'de' domain
    When I select 'industrial' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_industrial.csv' file

  Scenario: De - jewelry
    Given I navigate on 'de' domain
    When I select 'jewelry' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_jewelry.csv' file

  Scenario: De - appliances
    Given I navigate on 'de' domain
    When I select 'appliances' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_appliances.csv' file

  Scenario: De - lighting
    Given I navigate on 'de' domain
    When I select 'lighting' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_lighting.csv' file

  Scenario: De - dvd-bypos
    Given I navigate on 'de' domain
    When I select 'dvd-bypos' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_dvd-bypos.csv' file

  Scenario: De - luggage
    Given I navigate on 'de' domain
    When I select 'luggage' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_mluggage.csv' file

  Scenario: De - luxury-beauty
    Given I navigate on 'de' domain
    When I select 'luxury-beauty' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_luxury-beauty.csv' file

  Scenario: De - mi
    Given I navigate on 'de' domain
    When I select 'mi' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_mi.csv' file

  Scenario: De - videogames
    Given I navigate on 'de' domain
    When I select 'videogames' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_videogames.csv' file

  Scenario: De - pets
    Given I navigate on 'de' domain
    When I select 'pets' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_pets.csv' file

  Scenario: De - shoes
    Given I navigate on 'de' domain
    When I select 'shoes' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_shoes.csv' file

  Scenario: De - software
    Given I navigate on 'de' domain
    When I select 'software' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_software.csv' file

  Scenario: De - sports
    Given I navigate on 'de' domain
    When I select 'sports' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_sports.csv' file

  Scenario: De - office-products
    Given I navigate on 'de' domain
    When I select 'office-products' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_office-products.csv' file

  Scenario: De - toys
    Given I navigate on 'de' domain
    When I select 'toys' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_toys.csv' file

  Scenario: De - vhs
    Given I navigate on 'de' domain
    When I select 'vhs' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_vhs.csv' file

  Scenario: De - watches
    Given I navigate on 'de' domain
    When I select 'watches' category
    And I search all items
    And I filter by '10' price
    And I save results for each subcategory to 'de_watches.csv' file