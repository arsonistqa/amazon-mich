Given(/^I navigate on '(.*)' domain/) do |domain|
  @amazon = Amazon.new
  case domain
    when 'co.uk'
      visit (EnvConfig.get :co_uk_url).to_s
    when 'de'
      visit (EnvConfig.get :de_url).to_s
    when 'fr'
      visit (EnvConfig.get :fr_url).to_s
    when 'es'
      visit (EnvConfig.get :es_url).to_s
    when 'it'
      visit (EnvConfig.get :it_url).to_s
  end
end