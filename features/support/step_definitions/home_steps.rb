When /^I search all items$/ do
  @amazon.home_page.search_field.set '[]'
  @amazon.home_page.search_button.click
end

When /^I select '(.*)' category/ do |category|
  @amazon.home_page.select_category(category)
end

When /^I filter by '(.*)' price/ do |price|
  @amazon.home_page.lov_price_field.set price
  @amazon.home_page.go_button.click
end