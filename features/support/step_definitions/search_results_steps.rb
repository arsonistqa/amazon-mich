Then /^I save results for each subcategory to '(.*)' file$/ do |file_name|
  @amazon.search_results_page.get_data(file_name)
end